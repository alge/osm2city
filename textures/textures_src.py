#!/usr/bin/env python2

from textures import Texture as Texture
def import_us(facades):

  facades.append(Texture('tex.src/textures_us/commercial/50storySteelGlassmodern1.jpg',
      h_size_meters=27, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=108, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/45storyglassmodern.jpg',
      h_size_meters=40, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=80, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-OfficeModern-42st.jpg',
      h_size_meters=39, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=156, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/41storyconcrglasswhitemodern2.jpg',
      h_size_meters=20, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=80, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/40storymodern.jpg',
      h_size_meters=40, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=160, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/36storyconcrglassmodern.jpg',
      h_size_meters=32, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=128, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/35storyconcrmodernwhite.jpg',
      h_size_meters=25, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=100, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/30storyconcrbrown4.jpg',
      h_size_meters=29, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=58, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/28storymodern.jpg',
      h_size_meters=29, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=58, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/27storyConcrBrownGlass.jpg',
      h_size_meters=17, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=68, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/25storyBrownWide1.jpg',
      h_size_meters=60, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=60, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/20storybrownconcrmodern.jpg',
      h_size_meters=29, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=58, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/20storygreycncrglassmodern.jpg',
      h_size_meters=27, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=54, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/19storyretromodern.jpg',
      h_size_meters=30, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=120, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/18storyoffice.jpg',
      h_size_meters=28, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=56, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/16storyconcrglassgreymodern4.jpg',
      h_size_meters=14, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=28, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/15storybrownconcr.jpg',
      h_size_meters=38, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=76, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/15storyltbrownconcroffice3.jpg',
      h_size_meters=29, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=58, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/12storygovtmodern.jpg',
      h_size_meters=21, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=21, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/10storymodernconcrete.jpg',
      h_size_meters=27, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=54, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dcofficeconcrwhite8st.jpg',
      h_size_meters=24, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=24, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dchotelDC2_8st.jpg',
      h_size_meters=15, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=30, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dcofficeconcrwhite6-7st.jpg',
      h_size_meters=34, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=17, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/7storymodernsq.jpg',
      h_size_meters=19, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=19, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dcdupontconcr5st.jpg',
      h_size_meters=7, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=14, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/5storywhite.jpg',
      h_size_meters=12, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=12, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dcgovtconcrtan4st.jpg',
      h_size_meters=18, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=9, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/3storystorefronttown.jpg',
      h_size_meters=9, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=9, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/salmon_3_story_0_scale.jpg',
      h_size_meters=8, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=8, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/2stFancyconcrete1.jpg',
      h_size_meters=14, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=7, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dcwhiteconcr2st.jpg',
      h_size_meters=16, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=8, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-dctbrickcomm2st.jpg',
      h_size_meters=21, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=5.25, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/USUAE-4stCommercial.jpg',
      h_size_meters=20, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=10, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-OfficeComm-2st.jpg',
      h_size_meters=15, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=3.75, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-1stCommWarehousewhite1.jpg',
      h_size_meters=15, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=3.75, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-1stCommBrick2.jpg',
      h_size_meters=15, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=3.75, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/commercial/US-1stCommStFront3.jpg',
      h_size_meters=10, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=5, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/residential/tiles/USUAE-8stTile_rep.jpg',
      h_size_meters=15, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=30, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/residential/6storybrickbrown1.jpg',
      h_size_meters=21, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=21, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/residential/5storyCondo_concrglasswhite.jpg',
      h_size_meters=14, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=28, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/residential/US-CityCondo_brick_4st.jpg',
      h_size_meters=16, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=16, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))


  facades.append(Texture('tex.src/textures_us/residential/US-CityCondo2st.jpg',
      h_size_meters=11, h_cuts=[0, 500, 1000], h_can_repeat=False,
      v_size_meters=11, v_cuts=[0, 500, 1000], v_can_repeat=False,
      v_align_bottom=True, height_min=10,
      requires=['roof:colour:brown'],
      provides=['shape:urban', 'shape:commercial', 'age:modern', 'compat:roof-flat']))



  return facades
